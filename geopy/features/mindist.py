import numpy as np
from pylab import imread, imsave
import matplotlib.pyplot as plt
from scipy import ndimage
from math import sqrt

DEBUG = True

water_color = (153/255.0, 179/255.0, 204/255.0) # used in terrain maptype
water_color1 = (165/255.0, 191/255.0, 221/255.0) # used in roadmap maptype
green1 = (180/255.0, 211/255.0, 164/255.0)

def find_feature_dist(img, feature_color=water_color, center=(0.5, 0.5), ignore_size=3, debug=False):
    DEBUG=debug
    imgsave = img[0:-4]
    a = imread(img)
    ylen, xlen = np.shape(a)[:2]
    b = np.zeros((ylen, xlen))
    orig = xlen*center[0], ylen*center[1]

    
    pix_count = 0
    for j in xrange(ylen):
        for i in xrange(xlen):
            diff = sum(x*x for x in abs(a[j][i] - feature_color))

            if diff < 0.0001:
                pix_count += 1
                if DEBUG: print diff
                x, y = i+1-orig[0], j+1-orig[1]
                b[j][i] = x*x + y*y

    
    if DEBUG: imsave(imgsave+'-1.png', b)
    c = ndimage.grey_erosion(b, structure=np.ones((ignore_size, ignore_size))).astype(b.dtype)
    if DEBUG: imsave(imgsave+'-2.png', c)
    #mj, mi = c.argmin()/400, c.argmin()%400
    #print mj, mi
    
    curr_min, mi, mj = ylen*ylen + xlen*xlen, 0, 0
    for j in xrange(ylen):
        for i in xrange(xlen):
            v = c[j][i]
            if v > 0 and v < curr_min:
                curr_min = v
                mi, mj = i, j

    if DEBUG:
        print mi, mj, sqrt(curr_min)
        if mi > 390: mi = 390
        if mj > 390: mj = 390
        for j in xrange(10):
            for i in xrange(10):
                c[mj+j][mi+i] = 10000000
        imsave(imgsave+'-3.png', c)

    
    return sqrt(curr_min), pix_count


if __name__ == "__main__":
    import sys
    img = sys.argv[1]
    print find_feature_dist(img, feature_color=water_color, ignore_size=3)
